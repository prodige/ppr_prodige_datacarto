<?php

namespace ProdigeDataCarto\ServicesBundle\Models;

use Phalcon\Mvc\Model;

/**
 * User
 * catalogue.utilisateur_carte
 */
class UserMap extends Model
{
    public $id;
    public $idUser;
    public $idMap;

    /**
     * Columns mapping
     * @return array
     */
    public function columnMap()
    {
        return [
            'id'                => 'id',
            'fk_utilisateur'    => 'idUser',
            'fk_stockage_carte' => 'idMap',
        ];
    }

    /**
     * Association mapping
     */
    public function initialize() 
    {
        $this->setSchema('catalogue');
        $this->setSource('utilisateur_carte');
        $this->belongsTo('idUser', "ProdigeDataCarto\\ServicesBundle\\Models\\User", 'id', array('foreignKey' => TRUE));
        $this->belongsTo('idMap',  "ProdigeDataCarto\\ServicesBundle\\Models\\Map", 'id', array('foreignKey' => TRUE));
    }

}
