<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

// Cache the data for 2 days
/*$frontCache = new \Phalcon\Cache\Frontend\Data(["lifetime" => 86400, ]);
// Create the Cache setting memcached connection options
$cache = new \Phalcon\Cache\Backend\Memcache($frontCache, ["host" => "datacarto-memcached", "port" => 11211, "persistent" => false, ]);

$configCacheName = "dataCartoConfig";
//$cache->delete($configCacheName);
if( $cache->exists($configCacheName) ) {
  $prodigeConfig = $cache->get($configCacheName);
} else {*/
  $_fnEvalConfig = function(array $config) {
    foreach($config as $key => $value) {
      $config[$key] = preg_replace_callback(
        "/\%([^\%]+)\%/", 
        function($matches) use ($config) {
          return (isset($matches[1]) && isset($config[$matches[1]]) ? $config[$matches[1]] : $matches[0]);
        },
        $value
        );
    }
    return $config;
  };

  $prodigeParametersYaml = new \Phalcon\Config\Adapter\Yaml(BASE_PATH.'/global_parameters.yaml');
  $prodigeConfigSrc = $prodigeParametersYaml->toArray()["parameters"];
  //for local config
  
  if(file_exists(BASE_PATH.'/dns_config.yml')){
    $prodigeConfigDNSYaml  = new \Phalcon\Config\Adapter\Yaml(BASE_PATH.'/dns_config.yml');
    $prodigeConfigSrc = array_merge($prodigeConfigDNSYaml->toArray()["parameters"], $prodigeParametersYaml->toArray()["parameters"]);

  }
  $prodigeConfig = $_fnEvalConfig($prodigeConfigSrc);
  //$cache->save($configCacheName, $prodigeConfig);
//}

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Postgresql',
        'host'        => $prodigeConfig["catalogue_host"],
        'port'        => $prodigeConfig["catalogue_port"],
        'username'    => $prodigeConfig["catalogue_user"],
        'password'    => $prodigeConfig["catalogue_password"],
        'dbname'      => $prodigeConfig["catalogue_name"],
        'charset'     => $prodigeConfig["catalogue_charset"],
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'vendorDir'      => BASE_PATH . '/vendor/',
        
        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ],
    'prodigeConfig'  => $prodigeConfig,
]);
