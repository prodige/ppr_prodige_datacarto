<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
    ]
);


$loader->registerNamespaces(
  [
    "ProdigeDataCarto\ServicesBundle\Controller" => $config->application->controllersDir,
    "ProdigeDataCarto\ServicesBundle\Models" => $config->application->modelsDir,
  ]
);

$loader->register();
