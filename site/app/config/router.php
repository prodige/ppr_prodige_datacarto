<?php

use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router\Annotations as Annotations;

$di->set('annotations', function(){
    //In development mode the memory adapter could be used
    //it will read the annotations every time requested
    //other adapter would store the parsed annotations in files/shared memory, etc
    return new Phalcon\Annotations\Adapter\Memory();
});

$di->setShared("router",
    function() {
        // Use the annotations router
        $router = new Annotations(false);

        $router->setDefaultNamespace("ProdigeDataCarto\ServicesBundle\Controller");

        // This will do the same as above but only if the handled uri starts with /robots
        $router->addResource("ProdigeDataCarto\ServicesBundle\Controller\Service");

        return $router;
    }
);

$router = $di->getRouter();

//$router->addResource("Service");

$router->handle($_SERVER['REQUEST_URI']);
