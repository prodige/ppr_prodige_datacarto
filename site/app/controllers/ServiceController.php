<?php

namespace ProdigeDataCarto\ServicesBundle\Controller;

use ProdigeDataCarto\ServicesBundle\Controller\Traits\HttpTrait;
use ProdigeDataCarto\ServicesBundle\Controller\Traits\RequestTrait;
use ProdigeDataCarto\ServicesBundle\Controller\Traits\StatTrait;
use ProdigeDataCarto\ServicesBundle\Controller\Traits\LoaderUrlTrait;
use ProdigeDataCarto\ServicesBundle\Controller\Traits\UserTrait;

class ServiceController extends ControllerBase
{
    use HttpTrait;
    use RequestTrait;
    use StatTrait;
    use LoaderUrlTrait;
    use UserTrait;

    /**
     * Url d'initialisation de session utilisateur
     * Retourne en json, l'identifiant de connexion
     * @Route('/check-user',
     *        name='datacarto_check_user',
     *        methods={'GET'}
     * )
     */
    public function indexAction()
    {
      $data = array("userName" => $this->checkCurrentUser() );
      
      $this->sendJsonResponse($data);
      exit();
    }    
    
    /**
     * Requête vers le serveur cartographique
     * @Route('/map/{map:[a-zA-Z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_map_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/map',
     *         name='data_carto_map_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function proxyMapservAction($map="")
    {
        //$user           = $this->getCurrentUser();
        $logLevel       = $this->getLogLevel();
        
        $url            = $this->getRequestSchemeAndHttpHost()."/cgi-bin/mapserv";
        $urlParams      = $this->getRequestAllParamsByCurrentMethod();
        
        $method         = $this->request->getMethod();

        $pathMapFile = pathinfo($map);

        $pathMapFile["dirname"]  = ( isset($pathMapFile["dirname"]) ? $pathMapFile["dirname"] : "" );
        if( substr($pathMapFile["dirname"], 0, 1) == "/" || substr($pathMapFile["dirname"], 0, 1) == ".") {
          $pathMapFile["dirname"] = substr($pathMapFile["dirname"], 1);
        }
        if( $pathMapFile["dirname"]!="" && substr($pathMapFile["dirname"], -1) != "/" ) {
          $pathMapFile["dirname"] .= "/";
        }
        $pathMapFile["basename"]  = ( isset($pathMapFile["basename"]) ? $pathMapFile["basename"] : "" );
        $pathMapFile["extension"] = ( isset($pathMapFile["extension"]) ? $pathMapFile["extension"] : "map" );
        
        $mapPathFileName = $pathMapFile["dirname"].$pathMapFile["basename"].".".$pathMapFile["extension"];

        if(!empty($pathMapFile["basename"]) ) {
          
            if(true ||  $this->isUserHasRightOnMap($user, $mapPathFileName) ) {
                $urlParams["map"] = $this->getPathToMapfileDirectory()."/".$mapPathFileName;
              
                $this->loadUrl($url, $urlParams, $method, $logLevel, false, 65536, 15);
                return;
            } else {
              $this->SendHttpError(401, "No rights on this map.");
              return;
            }
        }
        
        $this->SendHttpError(500, "No parameters sent.");
    }
    
    /**
     * Requête wms externe (old version < prodige 4.1)
     * @Route('/WMS/{map:[a-zA-Z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_wms_old_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/WMS',
     *         name='data_carto_wms_old_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function wmsOldAction($map="")
    {
        $this->proxyWms($map);
    }
    
    /**
     * Requête wms externe
     * @Route('/wms/{map:[a-zA-Z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_wms_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/wms',
     *         name='data_carto_wms_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function wmsAction($map="")
    {
        $this->proxyWms($map);
    }

    /**
     * Requête wms d'un domaine
     * @Route('/wms-dom/{map:[a-z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_wms_domaine_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/wms-dom',
     *         name='data_carto_wms_domaine_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function wmsDomAction($map="")
    {
        $this->proxyWms($map, true);
    } 
    
    /**
     * Requête wms d'un sous-domaine
     * @Route('/wms_sdom/{map:[a-zA-Z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_wms_sousdomaine_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/wms_sdom',
     *         name='data_carto_wms_sousdomaine_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function wmsSDomAction($map="")
    {
        $this->proxyWms($map, false, true);
    } 
    
    /**
     * Requête wms d'un sous-domaine
     * @Route('/WMS_SDOM/{map:[a-zA-Z0-9\/:%_+.,\#?!@&=-]+}',
     *         name='data_carto_wms_sousdomaine_service_map',
     *         methods={'POST', 'GET'}
     * )
     * @Route('/WMS_SDOM',
     *         name='data_carto_wms_sousdomaine_service',
     *         methods={'POST', 'GET'}
     * )
     * @param string $map  nom de la carte
     * @return 
     */
    public function wmsSDomOldAction($map="")
    {
        $this->proxyWms($map, false, true);
    }
 
    /**
     * Proxy pour le flux wms
     * @param string $map
     * @param bool   $isDomain
     * @param bool   $isSubDomain
     */
    protected function proxyWms($map="", $isDomain=false, $isSubDomain=false)
    {
        $user           = $this->getCurrentUser();
        $logLevel       = $this->getLogLevel();
        $url            = $this->getRequestSchemeAndHttpHost()."/cgi-bin/mapserv";
        $urlParams      = $this->getRequestAllParamsByCurrentMethod();
        $emptyUrlParams = empty($urlParams);
        $method         = $this->request->getMethod();
        
        if( !$emptyUrlParams ) {
            $mapFile = ( !empty($map) ? basename($map) : "" );
            $partition = ( $isSubDomain ? "sdom_" : ( $isDomain ? "dom_" : "" ));

            $mapPathFileName = ( !empty($mapFile)
                                 ? "wms_".$partition.$mapFile.".map"
                                 : "wms.map");
            
            if( !$this->hasRequestParamWithValue('SERVICE') ) {
                $urlParams['SERVICE'] = "WMS";
            }
            
            $urlParams["map"] = $this->getPathToMapfileDirectory()."/".$mapPathFileName;

            if( $this->hasRequestParamWithValue("REQUEST", "getmap") ){
                $this->writeWxsStat($user, 'WMS');
            }

            $this->loadUrl($url, $urlParams, $method, $logLevel, false, 65536, 15);
            return;
        }
        
        $this->SendHttpError(500, "No parameters sent.");
    }
    
    /**
     * Proxy pour le flux wfs  (ancienne version < PRODIGE 4.1)
     * @Route('/WFS/{map:[a-z0-9\/:%_+.,\#?!@&=-]+}',
     *        name='datacarto_wfs_old_map',
     *        methods={'POST', 'GET'}
     * )
     * @Route('/WFS',
     *        name='datacarto_wfs_old',
     *        methods={'POST', 'GET'}
     * )
     */
    public function proxyWfsOldAction($map="")
    {
        $this->proxyWfsAction($map);
    }
    
    /**
     * Proxy pour le flux wfs 
     * @Route('/wfs/{map:[a-z0-9\/:%_+.,\#?!@&=-]+}',
     *        name='datacarto_wfs_map',
     *        methods={'POST', 'GET'}
     * )
     * @Route('/wfs',
     *        name='datacarto_wfs',
     *        methods={'POST', 'GET'}
     * )
     */
    public function proxyWfsAction($map="")
    {
        $user           = $this->getCurrentUser();
        $logLevel       = $this->getLogLevel();
        $url            = $this->getRequestSchemeAndHttpHost()."/cgi-bin/mapserv";
        $urlParams      = $this->getRequestAllParamsByCurrentMethod();
        
        $method         = $this->request->getMethod();
        
        $mapFile = ( !empty($map) ? basename($map) : "" );
        
        $mapPathFileName = ( !empty($mapFile)
                             ? "wfs_".$mapFile.".map"
                             : "wfs.map");
        
        $urlParams["map"] = $this->getPathToMapfileDirectory()."/".$mapPathFileName;


        if( !$this->hasRequestParamWithValue('SERVICE') ) {
            $urlParams['SERVICE'] = "WFS";
        }
        if( !!$this->hasRequestParamWithValue('VERSION') ) {
            $urlParams['VERSION'] = "1.0.0";
        }

        if( $this->hasRequestParamWithValue("REQUEST", "getfeature") ){
            // write logs only for layer call, ie not for GetCapabilities, describeFeature...
            $this->writeWxsStat($user, 'WFS', 'TYPENAME');
            $this->writeWxsStat($user, 'WFS', 'TYPENAMES');
        }

        $this->loadUrl($url, $urlParams, $method, $logLevel, true);
        return;
    }
    
    /**
     * Proxy pour le flux wmsc
     * @Route('/wmsc',
     *        name='datacarto_wmsc',
     *        methods={'POST', 'GET'}
     * )
     */
    public function proxyWmscAction()
    {
        $user      = $this->getCurrentUser();
        $logLevel  = $this->getLogLevel();
        $url       = $this->getRequestSchemeAndHttpHost()."/mapcache.fcgi/wmsc";
        $urlParams = $this->getRequestAllParamsByCurrentMethod();
        $method    = $this->request->getMethod();
        
        if( $this->hasRequestParamWithValue("REQUEST", "getmap") ){
            $this->writeWxsStat($user, 'WMSC');
        }
        
        $this->loadUrl($url, $urlParams, $method, $logLevel, false, 65536, 15);
    }
    
    /**
     * Proxy pour le flux wmts
     * @Route('/wmts',
     *        name='datacarto_wmts',
     *        methods={'POST', 'GET'}
     * )
     */
    public function proxyWmtsAction()
    {
        $user      = $this->getCurrentUser();
        $logLevel  = $this->getLogLevel();
        $url       = $this->getRequestSchemeAndHttpHost()."/mapcache.fcgi/wmts";
        $urlParams = $this->getRequestAllParamsByCurrentMethod();
        $method    = $this->request->getMethod();
        
        if( $this->hasRequestParamWithValue("REQUEST", "gettile") ) {
            //$this->writeWxsStat($user, 'WMTS');
        }
        
        $this->loadUrl($url, $urlParams, $method, $logLevel, false, 65536, 15);
    }
    
    /**
     * Proxy pour le flux wmts
     * @Route('/wms-tiled',
     *        name='datacarto_wms-tiled',
     *        methods={'POST', 'GET'}
     * )
     */
    public function proxyWmsTiledAction()
    {
        $user      = $this->getCurrentUser();
        $logLevel  = $this->getLogLevel();
        $url       = $this->getRequestSchemeAndHttpHost()."/mapcache.fcgi/wms";
        $urlParams = $this->getRequestAllParamsByCurrentMethod();
        $method    = $this->request->getMethod();
        
        if( $this->hasRequestParamWithValue("REQUEST", "gettile") ) {
            //$this->writeWxsStat($user, 'WMTS');
        }
        
        $this->loadUrl($url, $urlParams, $method, $logLevel, false, 65536, 15);
    }

}
