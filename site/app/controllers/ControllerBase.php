<?php

namespace ProdigeDataCarto\ServicesBundle\Controller;

use Phalcon\Mvc\Controller;

//use Phalcon\Http\Request;
//use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\Services\Logs;


class ControllerBase extends Controller
{
    const LOG_DEBUG_LEVEL = 7; //7;
    const LOG_INFO_LEVEL = 6;
  
  
    /**
     * Retourne le chemin physique du prépertoire app de l'application
     * @return string
     */
    protected function getRootDir()
    {
      return realpath(APP_PATH);
    }
  
    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToMapfileDirectory()
    {
      $config = $this->getDi()->getConfig();
      $rootPath = $config->path('prodigeConfig.PRODIGE_PATH_DATA');
      
    	return realpath($rootPath.'/cartes/Publication');
    }
    
    /**
     * Get the log path
     */
    protected function getLogFile() 
    {
      $config = $this->getDi()->getConfig();
      $logFile = $config->path('prodigeConfig.log_prod_path');
      return $logFile;
    }

    /**
     * Return log level 
     * >=7 debug, =6 info, cf https://github.com/phalcon/cphalcon/blob/master/phalcon/logger.zep
     * @return int
     */
    protected function getLogLevel()
    {
      $config = $this->getDi()->getConfig();

      if ( ! strcmp($config->path('prodigeConfig.log_prod_level'), 'debug')) {
        return self::LOG_DEBUG_LEVEL;
      } else { // Default info level
        return self::LOG_INFO_LEVEL;
      }
    }
}
