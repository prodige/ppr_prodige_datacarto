<?php

namespace ProdigeDataCarto\ServicesBundle\Controller\Traits;

use Phalcon\Logger\Adapter\Stream;
use Phalcon\Logger;
use ProdigeDataCarto\ServicesBundle\Models\User;

/**
 * StatTrait
 */
Trait StatTrait
{
    /**
     * Ecrit la stat de consultation dans 2 fichiers : log et csv
     * @param string $service
     * @param array  $data
     * @param string $delimLog  =',' par défaut pour le délimiteur de champ dans le fichier de log
     * @param string $delimStat =';' par défaut pour le délimiteur de champ dans le fichier de csv
     */
    protected function addStat($service, array $data, $delimLog=",", $delimStat=";")
    {
        $logPath = realpath($this->getDI()->getConfig()->path('prodigeConfig.PRODIGE_PATH_DATA')."logs");
        if( is_string($logPath) && !empty($logPath) ) {
            $logFile =  $logPath.'/'.$service.".log";
            $adapter = new Stream($logFile, ['mode' => 'w']);
            $logger = new Logger('messages', ['stat' => $adapter]);
            if( is_object($logger) ) {
                $logger->info($service.$delimLog.implode($delimLog, $data));
            }

            $statFile = fopen($logPath.'/'.$service.".csv", "a");
            if( is_resource($statFile) ) {
                fwrite($statFile, "\r\n".implode($delimStat, array_merge(array(date("Y-m-d H:i:s"), $service), $data)));
                fclose($statFile);
            }
        }
        
    }
  
    /**
     * Ecrit les logs des requetes du service $service (WFS, WMS, etc.) en Get
     * @param User      $user        utilisateur connecté
     * @param string    $service     nom du servive (WFS, WMTS, WMS, WMSC)
     * @param string    $layerParam  nom du paramètre contenant le nom de la couche
     */
    protected function writeWxsStat(User $user, $service='WFS', $layerParam="LAYERS") 
    {
        $layer = $this->getRequestParam($layerParam);
        if( $layer!="" ) {
            $this->addStat($service, array($user->getName(), $user->getFirstName(), $user->getUserName(), $layer));
        }
    }
}
