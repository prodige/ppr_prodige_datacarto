<?php

namespace ProdigeDataCarto\ServicesBundle\Controller\Traits;

use Phalcon\Logger;
use Phalcon\Logger\Adapter\Syslog;
use Phalcon\Logger\Adapter\Stream;

/**
 * LoaderUrlTrait
 *
 */
Trait LoaderUrlTrait
{
    protected $curl;
    protected $fStdOut;
    protected $logger;
    protected $statLoading;
    
    /**
     * Appelle l'url donnée et retourne le contenu sur la sortie standard
     * @param string    $url               url à appeler
     * @param array     $urlParams         paramètres de l'url
     * @param string    $method            =POST|GET|PUT|..., =GET par défaut
     * @param int       $logLevel          =6 pour le niveau info, 7 pour le niveau debug
     * @param bool      $streamingMode     =true par défaut, =false pour une lecture du flux en 1 fois
     * @param int       $bufferSize        =8096 par défaut, taille de buffer pour la lecture du contenu
     * @param int       $timeout           =30, temps en seconde pour libérer la connexion en cas de non réponse
     * @param string    $stdout            ='' par défaut pour identifier la sortie standard, peut prendre la valeur d'une des sorties possibles : nom de fichier, php://stdout, ...
     * @param bool      $killSessionAfter  =true par défaut pour détruire la session
     * @return bool
     */
    protected function loadUrl($url, array $urlParams, $method='GET', $logLevel=6, $streamingMode=false, $bufferSize=65536, $timeout=3000, $stdout='', $killSessionAfter=false)
    {
        $done = false;
        $this->fStdOut = null;
        $this->curl = null;
        $headers = array();
        
        $this->statLoading = array("totalSize" => 0, "uploadedSize" => 0);
        
        //$this->logger = new Syslog("proxyDataCarto");
        $adapter = new Stream($this->getLogFile(), ['mode' => 'w']);
        $this->logger = new Logger('messages', ['main' => $adapter]);
        $this->logger->log($logLevel, "Standard output opened.");

        // ouverture en écriture de sortie standard
        if( !empty($stdout) ) {
            $this->fStdOut = fopen($stdout, 'wb');
        }
        
        try {
            $this->curl = curl_init();
            $this->logger->log($logLevel, "Curl init : ".$url.", ".$method);
            if( !is_resource($this->curl) ) {
              throw new \Exception("Unable to init curl connexion : ", 500);
            }

            // vérification de l'url et récupération des entêtes
            //$headers = $this->getHeaderFromUrl($url, $urlParams, $method);
            //$this->logger->debug("Load headers from url : ".print_r($headers["info"]["url"], true));
            
            // lecture du contenu de l'url et redirection vers la sortie standard
            $this->logger->log($logLevel, "Start of content loading...");
            $this->getContentFromUrl($url, $urlParams, $method, $bufferSize, $timeout, $streamingMode);
            $this->logger->log($logLevel, "End of content loading.");
            $done = true;
            
        } catch( \Exception $e ) {
          $code = $e->getCode();
          $msg  = strip_tags($e->getMessage());
          $this->logger->error(sprintf("%s %s : %s", $code, ( isset(self::$httpStatusCodes[$code]) ? self::$httpStatusCodes[$code] : "" ), $msg));
          $this->sendHttpError($code, "");
        }
        
        if( is_resource($this->fStdOut) ) {
          fclose($this->fStdOut);
        }
        $this->fStdOut = null;
        $this->logger->debug("Standard output closed");
        
        if( is_resource($this->curl) ) {
          curl_close($this->curl);
        }
        $this->curl = null;
        $this->logger->debug("Curl closed");

        if( $killSessionAfter ) {
          $this->logger->debug("KillSession");
          $this->getDi()->getSession()->remove("phpCAS");
        }
        
        //$this->logger->commit();
        //$this->logger->close();
        
        exit();
    }
    
    /**
     * Vérifie l'url et récupère le header
     * Nécessite que la connexion curl soit initialisée
     * Retourne le tableau des entêtes, sinon
     * @param string $url        url 
     * @param array  $urlParams  paramètres de l'url
     * @param string $method     =POST|GET|PUT|...
     * @return array[headers, info]
     * @throws \Exception
     *
    protected function getHeaderFromUrl($url, array $urlParams, $method='GET', $timeout=30)
    {
        curl_reset($this->curl);
        
        $params = http_build_query($urlParams);
        $url .= ( $method=='GET' ? '?'.$params : '' );
        
        $optEnabled =
          curl_setopt($this->curl, CURLOPT_URL, $url)
          && curl_setopt($this->curl, CURLOPT_POST, ( $method=='POST' ? 1 : 0 ))
          && curl_setopt($this->curl, CURLOPT_PUT, ( $method=='PUT' ? 1 : 0 ))
          && curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, false)
          && curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, false)
          && curl_setopt($this->curl, CURLOPT_HEADER, true)
          && curl_setopt($this->curl, CURLOPT_NOBODY, true)
          && ( $method=='GET'  || $method!='GET' && curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params) )
          && curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout)
          ;
        if( !$optEnabled ) {
          throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($this->curl), curl_error($this->curl)), 500);
        }

        $ok = curl_exec($this->curl);
        if( $ok === false ) {
          throw new \Exception(sprintf("Unable to read url %s with method %s : [%s] %s", $url, $method, curl_errno($this->curl), curl_error($this->curl)), 500);
        }
        
        $infos = curl_getinfo($this->curl);
        if( $infos["http_code"] != 200 ) {
          throw new \Exception(sprintf("Http code received : %s from %s with method %s", $infos["http_code"], $url, $method), $infos["http_code"]);
        }
        return array("headers" => $headers, "info" => $infos);
    }*/
  
    /**
     * Ecrit data sur la sortie standard
     * @param string $data
     */
    protected function writeToStandardOutput($data)
    {
      if( is_resource($this->fStdOut) ) {
        fwrite($this->fStdOut, $data);
      } else {
        echo $data;
      }
    }
    
    /**
     * Fonction de retour asynchrone pour écrire le résultat sur la sortie standard
     * @param string $curl
     * @param string $data
     * @param int
     */
    protected function curlCallback($curl, $data) 
    {
        if( $this->statLoading["uploadedSize"] == 0 ) {
          // envoi le header
          $info = curl_getinfo($curl);
          $sent200 = $this->sendHttpHeader($info);
          if( !$sent200 ) {
            throw new \Exception($data, 404);
          }
          $this->logger->debug("Send headers : ".print_r(headers_list(), true));
        }
        
        $length = strlen($data);
        
        $this->statLoading["uploadedSize"] += $length;
        //$this->logger->debug(sprintf("Loading %d  / %d", $this->statLoading["uploadedSize"], $this->statLoading["totalSize"]));
        $this->logger->debug(sprintf("Loading %d / %d bytes ".(headers_sent() ? "sent" : "notsent"), $length, $this->statLoading["uploadedSize"]));
              
        if( is_resource($this->fStdOut) ) {
          fwrite($this->fStdOut, $data);
          fflush($this->fStdOut);
        } else {
          echo $data;
          flush();
        }
        // le transfert s'arretera lorsque la longueur 0 sera retournée
        return $length;
    }
  
    /**
     * Charge le contenu du lien fourni selon la méthode sélectionnée
     * Nécessite que la connexion curl soit initialisée
     * Retourne true ou lève une exception
     * @param string    $url           url 
     * @param array     $urlParams     paramètres de l'url
     * @param string    $method        =POST|GET|PUT|...
     * @param int       $bufferSize    taille de buffer pour la lecture du contenu
     * @param int       $timeout       temps en seconde pour libérer la connexion en cas de non réponse
     * @param bool      $streamingMode =true pour un chargement en streaming, =false pour une lecture du flux en 1 fois
     * @throws \Exception
     */
    protected function getContentFromUrl($url, array $urlParams, $method, $bufferSize, $timeout, $streamingMode)
    {
        curl_reset($this->curl);
        
        $params = http_build_query($urlParams);
        $url .= '?'.$params;
        
        $headers = array();
        
        foreach (getallheaders() as $name => $value) {
            if(strtolower($name)!=="accept-encoding"){
                $headers[] = $name.": ".$value;
            }
             
        }
        
        $optEnabled =
          curl_setopt($this->curl, CURLOPT_URL, $url)
          && curl_setopt($this->curl, CURLOPT_POST, $method=='POST')
          && curl_setopt($this->curl, CURLOPT_PUT, $method=='PUT') 
          //&& curl_setopt($this->curl, CURLOPT_BUFFERSIZE, $bufferSize)
          && curl_setopt($this->curl, CURLOPT_BINARYTRANSFER, true)
          && curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, !is_resource($this->fStdOut))
          && curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, false)
          && curl_setopt($this->curl, CURLOPT_HEADER, false)
          && curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false)
          && curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true)         
          && curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers)
          && curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout)
          && ( $method === 'GET'  || curl_setopt($this->curl, CURLOPT_POSTFIELDS, file_get_contents('php://input')) ) // send request body
          && ( !$streamingMode || $streamingMode && curl_setopt($this->curl, CURLOPT_WRITEFUNCTION, array($this, 'curlCallback')))
         ;
        if( !$optEnabled ) {
          throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($this->curl), curl_error($this->curl)), 500);
        }

        $ok = curl_exec($this->curl);
        if( !$ok ) {
          throw new \Exception(sprintf("1- Unable to read url %s with method %s : [%s] %s", $url, $method, curl_errno($this->curl), curl_error($this->curl)), 404);
        } elseif( !$streamingMode ) {
          $info = curl_getinfo($this->curl);
          $sent200 = $this->sendHttpHeader($info);
          if( !$sent200 ) {
            throw new \Exception(sprintf("2- Unable to read url %s with method %s : [%s] %s", $url, $method, curl_errno($this->curl), curl_error($this->curl)), 404);
          }
          $this->logger->debug("Send headers : ".print_r(headers_list(), true));
          echo $ok;
          flush();
        }
    }
}
