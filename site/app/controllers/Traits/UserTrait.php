<?php

namespace ProdigeDataCarto\ServicesBundle\Controller\Traits;

include_once(BASE_PATH."/vendor/jasig/phpcas/source/CAS.php");

use ProdigeDataCarto\ServicesBundle\Models\User;
use ProdigeDataCarto\ServicesBundle\Models\Map;
use ProdigeDataCarto\ServicesBundle\Models\UserMap;

/**
 * UserTrait
 */
Trait UserTrait
{
  
    /**
     * Retourne l'identifiant de la personne connectée
     * @return string
     */
    protected function getCurentUserName()
    {
        if( !class_exists("\\phpCAS") ) {
          throw new \Exception("La librairie phpCAS n'est pas installée ou inaccessible.", 500);
        }
        
        $config = $this->getDi()->getConfig();
        $casVersion          = CAS_VERSION_3_0;
        $casDebug            = ( $config->path('prodigeConfig.cas_debug', false) ? true : false );
        $casHost             = $config->path('prodigeConfig.cas_host', 'localhost');
        $casContext          = $config->path('prodigeConfig.cas_context', '/');
        $casPort             = $config->path('prodigeConfig.cas_port', '443')+0;
        $casServerValidation = ( $config->path('prodigeConfig.server_validation', false) ? true : false );
        $casCertPath         = $config->path('prodigeConfig.server_ca_cert_path', '');
        $casCallBackUrl      = $config->path('prodigeConfig.cas_callback_url', '');
        $casProxyChain       = $config->path('prodigeConfig.cas_proxy_chain', false);
        $casProxy            = true;
   
        if( $casDebug ) {
           $logFile = $this->getLogFile();
           \phpCAS::setDebug($logFile);
        }

        if ( $casProxy ) {
            \phpCAS::proxy($casVersion, $casHost, $casPort, $casContext);
            if( !empty($casCallBackUrl) ) {
              \phpCAS::setFixedCallbackURL($this->getRequestSchemeAndHttpHost() . "/" . $casCallBackUrl);
            }

            // Allow this client to be proxied
            if( $casProxyChain && is_array($casProxyChain) ) {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain($casProxyChain));
            } else {
                \phpCAS::allowProxyChain(new \CAS_ProxyChain_Any());
            }
        } else {
            \phpCAS::client($casVersion, $casHost, $casPort, $casContext);
        }
        
        // For production use set the CA certificate that is the issuer of the cert
        // on the CAS server and uncomment the line below
        if ($casCertPath != '') {
          \phpCAS::setCasServerCACert($casCertPath);
        }

        // For quick testing you can disable SSL validation of the CAS server.
        // THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
        // VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
        if( false === $casServerValidation ) {
          \phpCAS::setNoCasServerValidation();
        }

        $userName = \phpCAS::checkAuthentication();
        if( is_bool($userName) && $userName === false ) {
          $userName = $config->path('prodigeConfig.PRO_USER_INTERNET_USR_ID', 'vinternet');
        } else {
          $userName = \phpCAS::getUser();
        }
        return $userName;
    }

    /**
     * Force la vérification de l'identifiant de connexion auprès du CAS
     * Si non connecté, la session pour DataCarto est modifiée pour tromper phpCAS afin de ne pas lancer de requête de vérification auprès du serveur CAS.
     * Retourne l'identifiant de connexion
     * @return string
     */
    protected function checkCurrentUser()
    {
        $config = $this->getDi()->getConfig();
        $anonymousName = $config->path('prodigeConfig.PRO_USER_INTERNET_USR_ID', 'vinternet');
      
        $userName = $this->getCurentUserName();
        $phpCAS = $this->session->has('phpCAS')  ? $this->session->get('phpCAS') : array(); 
        if( ! isset($phpCAS ["user"]) ) {

            $userName = $anonymousName;
            $phpCAS["user"] = $userName;
            $phpCAS['unauth_count'] = 0;
            $phpCAS['auth_checked'] = false;
            
            $this->session->set('phpCAS', $phpCAS); 
            
        } 
        return $userName;
    }
    
    /**
     * initialise la session CAS par un appel interne
     * @param string $defaultUserName
     * @return string
     */
    protected function loadCurrentUser($defaultUserName)
    {
       
      
        $curl = curl_init();
        if( !is_resource($curl) ) {
            throw new \Exception("Unable to init curl connexion : ", 500);
        }
      
        $url = $this->getRequestSchemeAndHttpHost()."/check-user";
        
        $optEnabled =
            curl_setopt($curl, CURLOPT_URL, $url)
            && curl_setopt($curl, CURLOPT_BINARYTRANSFER, true)
            && curl_setopt($curl, CURLOPT_RETURNTRANSFER, true)
            && curl_setopt($curl, CURLOPT_FRESH_CONNECT, false)
            && curl_setopt($curl, CURLOPT_HEADER, false)
            && curl_setopt($curl, CURLOPT_TIMEOUT, 30)
            ;
        
        if( !$optEnabled ) {
          throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($curl), curl_error($curl)), 500);
        }

        $resultJson = curl_exec($curl);

        if( is_resource($curl) ) {
            curl_close($curl);
        }
        
        $resultArray = json_decode($resultJson, true);
        
        return ( isset($resultArray["userName"]) ? $resultArray["userName"] : $defaultUserName );
    }
    
    /**
     * Retourne l'entité de l'utilisateur connecté
     * @return User
     */
    protected function getCurrentUser()
    {
        $config = $this->getDi()->getConfig();
        $anonymousName = $config->path('prodigeConfig.PRO_USER_INTERNET_USR_ID', 'vinternet');
      
        
        
        $phpcasSession = $this->session->has('phpCAS')  ? $this->session->get('phpCAS') : array(); 
       
        if( isset($phpcasSession["user"]) ) {
            $userName = $phpcasSession["user"];
        } else {
            $userName = $this->loadCurrentUser($anonymousName);
            
            $phpcasSession["user"] =  $userName;
            $phpcasSession["unauth_count"] =  0;
            $phpcasSession["auth_checked"] =  true;
            
            $this->session->set('phpCas', $phpcasSession); 
            
        }

        $user = User::findFirst(["userName = :USERNAME:", "bind" => ['USERNAME' => $userName ]]);
        if( !is_object($user) ) {
            $user = User::findFirst(["userName = :USERNAME:", "bind" => ['USERNAME' => $anonymousName ]]);
        }
        if( !is_object($user) ) {
            throw new \Exception('Unable to find current user.', 500);
        }
        return $user;
    }
    
    /**
     * Vérifie les accès de l'utilisateur à la carte
     * @param User   $user utilisateur connecté
     * @param string $map  nom de la carte
     * @return bool
     */
    protected function isUserHasRightOnMap(User $currentUser, $mapPath)
    {
      //temporary remove rights check
      return true;
      $map = Map::findFirst(["pathName = :PATH:", "bind" => ['PATH' => $mapPath ]]);
      if( !is_object($map) ) {
        return false;
      }
      $userMap = UserMap::findFirst(
        [
            "idUser = :IDUSER: and idMap = :IDMAP:",
            "bind" => [
                "IDUSER" => $currentUser->getId(),
                "IDMAP"  => $map->getId(),
                ]
        ]
        );
      
      return ( !is_object($userMap) ? false : true );
    }
}
