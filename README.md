# Module de diffusion PRODIGE

Module de diffusion de services OGC de PRODIGE (technologie PHP/Phalcon), faisant office de proxy vers mapserver et collectant des statistiques d'utilisation des flux. 

### configuration

Modifier le fichier  global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- [Accès au service WMS](https://datacarto.prodige.internal/wms).
- [Accès au service WFS](https://datacarto.prodige.internal/wfs).
- [Accès au service WMTS](https://datacarto.prodige.internal/wmts).
