FROM docker.alkante.com/alk-apache:1-php7.3
ENV APP_TARGET php

# Upgrade
RUN apt-get update && apt-get upgrade -y

# Install php7.3-pgsql
RUN apt-get update ;\
    apt-get install -y --no-install-recommends php7.3-pgsql  postgresql-client

RUN curl -fsSL https://packagecloud.io/phalcon/stable/gpgkey | apt-key add -
RUN echo "deb [arch=amd64] https://packagecloud.io/phalcon/stable/debian/ buster main" > /etc/apt/sources.list.d/phalcon.list && apt update

# PHP module
RUN apt-get update ;\
    apt install -y --no-install-recommends php7.3-yaml php7.3-cli php-cas php7.3-memcache php7.3-http php7.3-phalcon

# PHP module : fix psr
RUN cd /tmp &&\
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/php7.3-psr_1.2.0-5+0~20231125.21+debian10~1.gbp0e690b_amd64.deb && \
  apt-get update && \
  apt install -y --no-install-recommends ./php7.3-psr_1.2.0-5+0~20231125.21+debian10~1.gbp0e690b_amd64.deb &&\
  rm -f ./php7.3-psr_1.2.0-5+0~20231125.21+debian10~1.gbp0e690b_amd64.deb

RUN mkdir -p /var/www/html/tiles; \
  chown www-data: /var/www/html/tiles

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Configure fcgid apache module
RUN apt-get install -y --no-install-recommends libapache2-mod-fcgid
COPY --chown=root:root cicd/build/docker-prod/web/fcgid.conf /etc/apache2/mods-available/fcgid.conf
RUN a2enmod fcgid

# Install gdal
RUN cd /tmp && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/libecwj2_3.3-1buster_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/libgdal20_2.4.0+dfsg-1_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/gdal-bin_2.4.0+dfsg-1_amd64.deb && \
  wget -nv https://ftp.alkante.com/gitlab/alkante/prodige/buster/gdal-data_2.4.0+dfsg-1_all.deb && \
  apt-get update && \
  apt install -y --no-install-recommends ./gdal-bin_2.4.0+dfsg-1_amd64.deb ./gdal-data_2.4.0+dfsg-1_all.deb ./libecwj2_3.3-1buster_amd64.deb ./libgdal20_2.4.0+dfsg-1_amd64.deb && \
  apt-mark hold gdal-data gdal-bin libecwj2 libgdal20 &&\
  rm -f ./gdal-bin_2.4.0+dfsg-1_amd64.deb ./gdal-data_2.4.0+dfsg-1_all.deb ./libecwj2_3.3-1buster_amd64.deb ./libgdal20_2.4.0+dfsg-1_amd64.deb ;

# Install mapserv
RUN echo 'deb http://archive.debian.org/debian buster-backports main contrib non-free' > /etc/apt/sources.list.d/debian_backports.list &&\
    echo 'Package: *\n\
Pin: release o=Debian Backports,a=buster-backports,n=buster-backports,l=Debian Backports \n\
Pin-Priority: 400' > /etc/apt/preferences.d/debian_backports.pref &&\
    apt-get update &&\
    apt install -y --no-install-recommends -t buster-backports mapserver-bin php-mapscript &&\
    apt-mark hold mapserver-bin php-mapscript

RUN cp /usr/bin/mapserv /usr/lib/cgi-bin/mapserv; \
  cp /usr/bin/mapserv /usr/lib/cgi-bin/mapservwfs; \
  cp /usr/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi;

# Install mapcache
RUN apt update; \
  apt install -y mapcache-cgi mapcache-tools libapache2-mod-mapcache

COPY --chown=root:root cicd/build/docker-prod/web/epsg /usr/share/proj/epsg

RUN chmod o+r /usr/share/proj/epsg

RUN a2enmod cgi; \
  a2enmod cgid

# Tunning apache2 for prodige datacarto
RUN echo '\n\
ServerLimit             5000\n\
StartServers               10\n\
MaxRequestWorkers       5000\n\
MaxConnectionsPerChild      0\n'\
>> /etc/apache2/apache2.conf

# Copy Apache config
COPY --chown=root:root cicd/build/docker-prod/web/apache.conf /etc/apache2/sites-available/php.conf

# Copy src
COPY --chown=www-data:www-data jenkins_release/ppr_prodige_datacarto /var/www/html

# End
RUN apt-get clean ;\
    rm -rf /tmp/* /var/tmp/*

#rights
RUN if [ -f ./rights.sh ]; then \
   /bin/bash ./rights.sh; \
   mv rights.sh build_rights.sh ;\
   fi
